import sbtassembly.{MergeStrategy, PathList}


name := "SparkScalaPI"

version := "1.2"

scalaVersion := "2.11.11"

val sparkVersion = "2.1.0"
resolvers +=
  "sonatype-forge" at "https://repository.sonatype.org/content/groups/forge/"

resolvers +=
  "redshift" at "http://redshift-maven-repository.s3-website-us-east-1.amazonaws.com/release"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-streaming" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-hive" % sparkVersion % "provided",
  "com.databricks" %% "spark-redshift" % "2.0.1",
  "com.amazonaws" % "aws-java-sdk-redshift" % "1.11.126",
  "com.amazon.redshift" % "redshift-jdbc42" % "1.2.1.1001",
  "com.databricks" % "spark-avro_2.11" % "3.2.0",
  "com.amazonaws" % "aws-java-sdk" % "1.11.127"
)

mainClass in(Compile, run) := Some("SimpleApp")

assemblyMergeStrategy in assembly := {
  case PathList("com", "esotericsoftware", xs@_*) => MergeStrategy.last
  case PathList("org", "codehaus", xs@_*) => MergeStrategy.last
  case PathList("commons-beanutils", "codehaus", xs@_*) => MergeStrategy.last
  case PathList("org", "objenesis", xs@_*) => MergeStrategy.last
  case PathList("com", "squareup", xs@_*) => MergeStrategy.last
  case PathList("com", "sun", xs@_*) => MergeStrategy.last
  case PathList("com", "thoughtworks", xs@_*) => MergeStrategy.last
  case PathList("commons-beanutils", xs@_*) => MergeStrategy.last
  case PathList("commons-cli", xs@_*) => MergeStrategy.last
  case PathList("commons-collections", xs@_*) => MergeStrategy.last
  case PathList("commons-io", xs@_*) => MergeStrategy.last
  case PathList("io", "netty", xs@_*) => MergeStrategy.last
  case PathList("javax", "activation", xs@_*) => MergeStrategy.last
  case PathList("javax", "xml", xs@_*) => MergeStrategy.last
  case PathList("org", "apache", xs@_*) => MergeStrategy.last
  case PathList("org", "codehaus", xs@_*) => MergeStrategy.last
  case PathList("org", "fusesource", xs@_*) => MergeStrategy.last
  case PathList("org", "mortbay", xs@_*) => MergeStrategy.last
  case PathList("org", "tukaani", xs@_*) => MergeStrategy.last
  case PathList("org", "avro-ipc", xs@_*) => MergeStrategy.last
  case PathList("xerces", xs@_*) => MergeStrategy.last
  case PathList("xmlenc", xs@_*) => MergeStrategy.last
  case "about.html" => MergeStrategy.rename
  case "META-INF/ECLIPSEF.RSA" => MergeStrategy.last
  case "META-INF/mailcap" => MergeStrategy.last
  case "META-INF/mimetypes.default" => MergeStrategy.last
  case "plugin.properties" => MergeStrategy.last
  case "log4j.properties" => MergeStrategy.last
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}