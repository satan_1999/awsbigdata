** Original document

at https://aws.amazon.com/blogs/big-data/powering-amazon-redshift-analytics-with-apache-spark-and-amazon-machine-learning/

** Create Proxy to access the master of the cluster

ssh -i ~/Downloads/wallace2.pem -ND 8157 hadoop@ec2-54-149-148-19.us-west-2.compute.amazonaws.com

** Yarn Page ( Need to add froxProxy into browser )

http://ec2-54-149-148-19.us-west-2.compute.amazonaws.com:8088/

** Create EMR Cluster

aws emr create-cluster \
--name "Spark Cluster" \
--release-label emr-5.5.0 \
--instance-groups InstanceGroupType=MASTER,InstanceCount=1,InstanceType=m4.xlarge InstanceGroupType=CORE,InstanceCount=2,InstanceType=m4.xlarge \
--service-role EMR_DefaultRole \
--ec2-attributes InstanceProfile=EMR_EC2_DefaultRole,KeyName=wallace2,AdditionalMasterSecurityGroups=sg-bea531c5 \
--log-uri "s3://wallace-test/log/" \
--enable-debugging \
--no-auto-terminate \
--visible-to-all-users \
--applications Name=Hadoop Name=Hive Name=Spark \


** Submit step into the EMR(Spark) Cluster
aws emr add-steps --cluster-id j-19W4W8V6Y0X7V  --steps Type=Spark,MainClass=CopyDataFromHiveToRedshift,Name="ReadHive",ActionOnFailure=CONTINUE,Args=--deploy-mode,cluster,--class,CopyDataFromHiveToRedshift,s3://wallace-test/scala/SparkScalaPI-assembly-1.2.jar


aws emr add-steps --cluster-id j-19W4W8V6Y0X7V  --steps Type=Spark,MainClass=CopyDataFromHiveToRedshift,Name="ReadHive",ActionOnFailure=CONTINUE,Args=--deploy-mode,cluster,--class,CopyFileFromS3,s3://wallace-test/scala/SparkScalaPI-assembly-1.2.jar