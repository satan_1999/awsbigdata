CREATE EXTERNAL TABLE IF NOT EXISTS weather_raw (
  station       string,
  elevation     string,
  latitude      string,
  longitude     string,
  wdate         string,
  prcp          decimal(5,1),
  snow          int,
  tmax          string,
  tmin          string,
  awnd          string
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'
LOCATION 's3://wallace-test/weather/'
TBLPROPERTIES("skip.header.line.count"="1");


copy ord_flights
FROM 's3://wallace-test/flight/T_ONTIME.csv’
credentials 'aws_access_key_id=AKIAIOADYM7PHFVWXXHQ;aws_secret_access_key=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'
delimiter '|' lzop;
;


CREATE TABLE weather AS SELECT station, elevation, latitude, longitude,
                   cast(concat(
                                substr(wdate,1,4), '-',
                                substr(wdate,5,2), '-',
                                substr(wdate,7,2)
                              ) AS date) AS dt, prcp, snow, tmax, tmin, awnd FROM weather_raw;
