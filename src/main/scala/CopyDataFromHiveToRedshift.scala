/* SimpleApp.scala */


import org.apache.log4j.{Level, LogManager}
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions.udf

object CopyDataFromHiveToRedshift {
  def main(args: Array[String]) {
    //original document at https://aws.amazon.com/blogs/big-data/powering-amazon-redshift-analytics-with-apache-spark-and-amazon-machine-learning/
    println("CopyDataFromHiveToRedshift is started")
    val log = LogManager.getRootLogger
    log.setLevel(Level.WARN)

    // Creation of SparContext and SQLContext
    val spark = SparkSession.builder.appName("example-spark-scala-read-and-write-from-hive").enableHiveSupport().getOrCreate()

    val awsAccessKey = "AKIAIOADYM7PHFVWXXHQ"
    val awsSecretKey = "yevaa2V3I+OoV9AEDfciZbPDrxuC1feUb30xdp4z"

    spark.sparkContext.hadoopConfiguration.set("fs.s3n.awsAccessKeyId", awsAccessKey)
    spark.sparkContext.hadoopConfiguration.set("fs.s3n.awsSecretAccessKey", awsSecretKey)
    spark.sparkContext.hadoopConfiguration.set("fs.s3a.access.key", awsAccessKey)
    spark.sparkContext.hadoopConfiguration.set("fs.s3a.secret.key", awsSecretKey)

    try {
      // Read weather table from hive
      val rawWeatherDF = spark.sql("select * from weather")
      // Retrieve the header
      val header = rawWeatherDF.first()


      // Remove the header from the dataframe
      val noHeaderWeatherDF = rawWeatherDF.filter(row => row != header)
      println("Load rows from Weather except the header - " + noHeaderWeatherDF.count())
      // UDF to convert the air temperature from celsius to fahrenheit
      val toFahrenheit = udf { (c: Double) => c * 9 / 5 + 32 }

      // Apply the UDF to maximum and minimum air temperature
      val weatherDF = noHeaderWeatherDF.withColumn("new_tmin", toFahrenheit(noHeaderWeatherDF("tmin")))
        .withColumn("new_tmax", toFahrenheit(noHeaderWeatherDF("tmax")))
        .drop("tmax")
        .drop("tmin")
        .withColumnRenamed("new_tmax", "tmax")
        .withColumnRenamed("new_tmin", "tmin")

      weatherDF.printSchema()


      // Provide the jdbc url for Amazon Redshift
      val jdbcURL = "jdbc:redshift://wallacetest1.c6d6sv3ng1zi.us-west-2.redshift.amazonaws.com:5439/wallacetest?user=wallace_test_0_m&password=wallace_test_0_M"

      // Create and declare an S3 bucket where the temporary files are written
      val s3TempDir = "s3a://wallace-test/temp/"
      val flightsQuery =
        """
                    select id as ORD_DELAY_ID, DAY_OF_MONTH, DAY_OF_WEEK, FL_DATE, f_days_from_holiday(year, month, day_of_month) as DAYS_TO_HOLIDAY, UNIQUE_CARRIER, FL_NUM, substring(DEP_TIME, 1, 2) as DEP_HOUR, cast(DEP_DEL15 as smallint),
                    cast(AIR_TIME as integer), cast(FLIGHTS as smallint), cast(DISTANCE as smallint)
                    from ord_flights where origin='ORD' and cancelled = 0
                   """

      // Create a Dataframe to hold the results of the above query
      val flightsDF = spark.read.format("com.databricks.spark.redshift")
        .option("url", jdbcURL)
        .option("tempdir", s3TempDir)
        .option("query", flightsQuery)
        .load()

      println("Loaded " + flightsDF.count() + " rows")

      flightsDF.printSchema()

      val joinedDF = flightsDF.join(weatherDF, flightsDF("fl_date") === weatherDF("dt"))

      joinedDF.printSchema()
      println("Loaded " + joinedDF.count() + " rows")

      joinedDF.write
        .format("com.databricks.spark.redshift")
        .option("url", jdbcURL)
        .option("dbtable", "ord_flights_weather")
        .option("tempdir", s3TempDir)
        .mode(SaveMode.Overwrite)
        .save()

      spark.close()
    } catch {
      case e: Exception => log.error("Had a error", e)
    }
  }
}