/* SimpleApp.scala */


import org.apache.log4j.{Level, LogManager}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.udf
import org.apache.spark.{SparkConf, SparkContext}

object ReadHive {
  def main(args: Array[String]) {
    println("ReadHive is started")
    val log = LogManager.getRootLogger
    log.setLevel(Level.WARN)
    // Configuration of SparkContext
    val conf = new SparkConf().setAppName("example-spark-scala-read-and-write-from-hive")

    // Creation of SparContext and SQLContext
    val sc = new SparkContext(conf)
    val sqlContext = SparkSession.builder.appName("example-spark-scala-read-and-write-from-hive").enableHiveSupport().getOrCreate()
    // Configuring hiveContext to find hive metastore
    //    hiveContext.setConf("hive.metastore.warehouse.dir", params.hiveHost + "user/hive/warehouse")

    try {
      // Read weather table from hive
      val rawWeatherDF = sqlContext.sql("select * from weather limit 100")
      // Retrieve the header
      val header = rawWeatherDF.first()


      // Remove the header from the dataframe
      val noHeaderWeatherDF = rawWeatherDF.filter(row => row != header)
      log.info("Load rows from Weather except the header - " + noHeaderWeatherDF.count())
      // UDF to convert the air temperature from celsius to fahrenheit
      val toFahrenheit = udf { (c: Double) => c * 9 / 5 + 32 }

      // Apply the UDF to maximum and minimum air temperature
      val weatherDF = noHeaderWeatherDF.withColumn("new_tmin", toFahrenheit(noHeaderWeatherDF("tmin")))
        .withColumn("new_tmax", toFahrenheit(noHeaderWeatherDF("tmax")))
        .drop("tmax")
        .drop("tmin")
        .withColumnRenamed("new_tmax", "tmax")
        .withColumnRenamed("new_tmin", "tmin")

      log.info("Finished tweaking data")


      //save the new data into new weather table
      //    val newWeatherDF = weatherDF.clone().
      // Parquet files can also be used to create a temporary view and then used in SQL statements
      weatherDF.createOrReplaceTempView("newWeatherTempView")
      val namesDF = sqlContext.sql("create table newWeather as select * from newWeatherTempView")

      log.info("Writing hive table : OK")

      // ======= Reading files
      // Reading hive table into a Spark Dataframe
      val dfHive = sqlContext.sql("SELECT * from newWeather")
      log.info("Reading hive table newWeather : OK")
      log.info(dfHive.show())

      sqlContext.close()
    } catch {
      case e: Exception => log.error("Had a error", e)
    }

  }

}